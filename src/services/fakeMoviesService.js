import * as genresApis from "./fakeGenreService";

let movies = [
  {
    _id: "1",
    title: "Gadar",
    genre: {
      _id: "1",
      name: "Action"
    },
    numberInStock: 6,
    daulyRentalRate: 2.5,
    publishDate: "2018-12-10"
  },
  {
    _id: "2",
    title: "Hera-Feri",
    genre: {
      _id: "2",
      name: "Comady"
    },
    numberInStock: 9,
    daulyRentalRate: 4.5,
    publishDate: "2018-11-10"
  },
  {
    _id: "3",
    title: "Veer",
    genre: {
      _id: "1",
      name: "Action"
    },
    numberInStock: 15,
    daulyRentalRate: 1.5,
    publishDate: "2018-10-10"
  },
  {
    _id: "4",
    title: "Love-Aajkal",
    genre: {
      _id: "3",
      name: "Romance"
    },
    numberInStock: 2,
    daulyRentalRate: 5.5,
    publishDate: "2018-02-10"
  },
  {
    _id: "5",
    title: "Yuva",
    genre: {
      _id: "3",
      name: "Romance"
    },
    numberInStock: 10,
    daulyRentalRate: 0.5,
    publishDate: "2018-05-10"
  },
  {
    _id: "6",
    title: "Gadar",
    genre: {
      _id: "1",
      name: "Action"
    },
    numberInStock: 6,
    daulyRentalRate: 2.5,
    publishDate: "2018-12-10"
  },
  {
    _id: "7",
    title: "Hera-Feri",
    genre: {
      _id: "2",
      name: "Comady"
    },
    numberInStock: 9,
    daulyRentalRate: 4.5,
    publishDate: "2018-11-10"
  },
  {
    _id: "8",
    title: "Veer",
    genre: {
      _id: "1",
      name: "Action"
    },
    numberInStock: 15,
    daulyRentalRate: 1.5,
    publishDate: "2018-10-10"
  },
  {
    _id: "9",
    title: "Love-Aajkal",
    genre: {
      _id: "3",
      name: "Romance"
    },
    numberInStock: 2,
    daulyRentalRate: 5.5,
    publishDate: "2018-02-10"
  },
  {
    _id: "10",
    title: "Yuva",
    genre: {
      _id: "3",
      name: "Romance"
    },
    numberInStock: 10,
    daulyRentalRate: 0.5,
    publishDate: "2018-05-10"
  }
];
export function getMovies() {
  return movies;
}
export function getMovie(id) {
  return movies.find(movie => movie._id === id);
}
export function saveMovie(movie) {
  let movieInDb = movies.find(m => m._id === movie._id) || {};
  movieInDb.title = movie.title;
  movieInDb.genre = genresApis.genres.find(g => g._id === movie.genreId);
  movieInDb.numberInStock = movie.numberInStock;
  movieInDb.daulyRentalRate = movie.daulyRentalRate;

  if (!movieInDb._id) {
    movieInDb._id = Date.now().toString();
    movies.push(movieInDb);
  }
  return movieInDb;
}
