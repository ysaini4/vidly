import React, { Component } from "react";
import { toast } from "react-toastify";
import { getMovies } from "../services/fakeMoviesService";
import { getGenres } from "../services/fakeGenreService";
import MoviesTable from "./moviesTable";
import Pagination from "./common/pagination";
import ListGroup from "./common/listGroup";
import SearchBox from "./common/searchBox";
import { paginate } from "../utils/paginate";
import _ from "lodash";
import { Link } from "react-router-dom";

class Movies extends Component {
  state = {
    movies: [],
    genres: [],
    currentPage: 1,
    pageSize: 8,
    sortColumn: { path: "title", order: "asc" },
    selectedGenre: this.allGenreId(),
    searchQuery: ""
  };
  allGenreId() {
    return {
      _id: "",
      name: "All Genres"
    };
  }
  componentDidMount() {
    const genres = [this.allGenreId(), ...getGenres()];
    this.setState({ movies: getMovies(), genres });
  }
  handleDelete = movie => {
    const movies = this.state.movies.filter(mv => mv._id !== movie._id);
    this.setState({ movies });
  };
  handleLike = movie => {
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index] = { ...movies[index] };
    movies[index] = movie;
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };
  handleGenreSelect = genre => {
    const selectedGenre = genre;
    this.setState({ selectedGenre, currentPage: 1, searchQuery: "" });
  };
  handlePageChange = currentPage => {
    if (
      currentPage < 1 ||
      (currentPage - 1) * this.state.pageSize >= this.state.movies.length
    )
      return;
    this.setState({ currentPage });
  };
  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  getPageData = () => {
    const {
      pageSize,
      currentPage,
      movies: allMovies,
      selectedGenre,
      sortColumn,
      searchQuery
    } = this.state;
    let filtered =
      selectedGenre && selectedGenre._id
        ? allMovies.filter(movie => {
            if (movie.genre._id === selectedGenre._id) return true;
            return false;
          })
        : searchQuery
        ? allMovies.filter(movie => {
            if (movie.title.toLowerCase().includes(searchQuery.toLowerCase()))
              return true;
            return false;
          })
        : allMovies;
    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    const totalCount = sorted.length;
    const data = paginate(sorted, currentPage, pageSize);
    return { totalCount, data };
  };
  handelSearch = query => {
    this.setState({
      searchQuery: query,
      selectedGenre: this.allGenreId(),
      currentPage: 1
    });
  };
  render() {
    const { length: count } = this.state.movies;
    const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
    if (count === 0) return <p>There is no movie</p>;
    const { totalCount, data: movies } = this.getPageData();
    return (
      <div className="row">
        <div className="col-sm-3">
          <ListGroup
            items={this.state.genres}
            onItemSelect={this.handleGenreSelect}
            selectedItem={this.state.selectedGenre}
          />
        </div>
        <div className="col-sm-9">
          <Link
            className="btn btn-primary"
            to="/movies/new"
            style={{ marginBottom: 20 }}
          >
            New Movies
          </Link>
          <SearchBox searchQuery={searchQuery} onChange={this.handelSearch} />
          <p>Showing {totalCount} movies in database</p>
          <MoviesTable
            movies={movies}
            onDelete={this.handleDelete}
            onLike={this.handleLike}
            onSort={this.handleSort}
            sortColumn={sortColumn}
          />
          <Pagination
            itemCount={totalCount}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>
      </div>
    );
  }
}

export default Movies;
