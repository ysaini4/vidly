import React, { Component } from "react";
import Like from "./common/like";
import Table from "./common/table";
import { Link } from "react-router-dom";

class MovieTable extends Component {
  columns = [
    {
      lable: "Title",
      path: "title",
      content: movie => <Link to={`/movies/${movie._id}`}>{movie.title}</Link>
    },
    { lable: "Genre", path: "genre.name" },
    { lable: "Stock", path: "numberInStock" },
    { lable: "Rate", path: "daulyRentalRate" },
    {
      key: "key1",
      content: movie => (
        <Like liked={movie.liked} onClick={() => this.props.onLike(movie)} />
      )
    },
    {
      key: "key2",
      content: movie => (
        <button
          className="btn btn-danger btn-sm"
          onClick={() => this.props.onDelete(movie)}
        >
          Delete
        </button>
      )
    }
  ];
  render() {
    const { movies } = this.props;
    return (
      <Table
        sortColumn={this.props.sortColumn}
        onSort={this.props.onSort}
        columns={this.columns}
        data={movies}
      />
    );
  }
}
export default MovieTable;
