import React from "react";
import _ from "lodash";
import propTypes from "prop-types";
const Pagination = props => {
  const { itemCount, pageSize, onPageChange, currentPage } = props;
  const pageCount = Math.ceil(itemCount / pageSize);
  if (pageCount === 1) return null;
  const pages = _.range(1, pageCount + 1);
  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination">
        <li className="page-item">
          <span
            className="page-link"
            onClick={() => onPageChange(currentPage - 1)}
          >
            Previous
          </span>
        </li>
        {pages.map(pNum => {
          return (
            <li
              key={pNum}
              className={
                currentPage === pNum ? "page-item active" : "page-item"
              }
            >
              <span className="page-link" onClick={() => onPageChange(pNum)}>
                {pNum}
              </span>
            </li>
          );
        })}
        <li className="page-item">
          <span
            className="page-link"
            onClick={() => onPageChange(currentPage + 1)}
          >
            Next
          </span>
        </li>
      </ul>
    </nav>
  );
};
Pagination.propTypes = {
  itemCount: propTypes.number.isRequired,
  pageSize: propTypes.number.isRequired,
  onPageChange: propTypes.func.isRequired,
  currentPage: propTypes.number.isRequired
};
export default Pagination;
