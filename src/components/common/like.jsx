import React from "react";

const Like = props => {
  const { liked, onClick } = props;
  let className = "fa fa-heart";
  if (!liked) className += "-o";
  return (
    <i style={{ cursor: "pointer" }} className={className} onClick={onClick} />
  );
};

export default Like;
