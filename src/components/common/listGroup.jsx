import React from "react";

const ListGroup = props => {
  const {
    items,
    selectedItem,
    onItemSelect,
    textProperty,
    valueProperty
  } = props;
  let listclassd = "list-group-item list-group-item-action ";
  return (
    <ul className="list-group">
      {items.map(item => (
        <li
          key={item[valueProperty]}
          style={{ cursor: "pointer" }}
          className={
            item[textProperty] === selectedItem[textProperty]
              ? listclassd + "active"
              : listclassd
          }
          onClick={() => onItemSelect(item)}
        >
          {item[textProperty]}
        </li>
      ))}
    </ul>
  );
};
ListGroup.defaultProps = {
  textProperty: "name",
  valueProperty: "_id"
};
export default ListGroup;
