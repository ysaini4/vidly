import React from "react";
const SearchBox = ({ searchQuery, onChange }) => {
  return (
    <input
      type="text"
      value={searchQuery}
      className="form-control my-3"
      placeholder="Search..."
      onChange={e => onChange(e.currentTarget.value)}
    />
  );
};

export default SearchBox;
