import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import NavBar from "./components/navBar";
import Movies from "./components/Movies";
import Customers from "./components/customers";
import Rental from "./components/rental";
import NotFound from "./components/notFound";
import MoviesForm from "./components/moviesForm";
import LoginForm from "./components/loginForm";
import RegisterFrom from "./components/registerForm";
class App extends Component {
  state = {
    name: "yogy"
  };

  render() {
    return (
      <React.Fragment>
        <ToastContainer />
        <main className="container">
          <NavBar />
          <Switch>
            <Route path="/login" component={LoginForm} />
            <Route path="/register" component={RegisterFrom} />
            <Route
              path="/movies/:id"
              render={props => <MoviesForm {...props} />}
            />
            <Route path="/movies" component={Movies} />
            <Route path="/customers" component={Customers} />
            <Route path="/rentels" component={Rental} />
            <Route path="/not-found" component={NotFound} />
            <Redirect from="/" to="/movies" exact />
            <Redirect to="/not-found" />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
